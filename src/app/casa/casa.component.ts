import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Casa } from '../casa';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-casa',
  templateUrl: './casa.component.html',
  styleUrls: ['./casa.component.css']
})
export class CasaComponent implements OnInit {

  casaSelecionada: number = 0;
  
  casas: Casa[] = [];
  
  constructor(private service: AppService, private router: Router) {
  }
  
  ngOnInit(): void {
    this.findAll()
  }

  findAll(): void {
    this.service.findAll().subscribe((retorno) => {
      this.casas = retorno
      this.casaSelecionada = this.casas[0].id
    })
  }

  editar() {
    console.log('teste', this.casaSelecionada)
    console.log(this.casaSelecionada)
    this.service.findById(this.casaSelecionada).subscribe((retorno) =>{
      console.log('retorno', retorno)
    })
    this.router.navigate([`detalhes/${this.casaSelecionada}`])
  }


  navegarParaDetalhes(): void {
    console.log(this.casaSelecionada)
    this.router.navigate(['detalhes'])
  }


}
