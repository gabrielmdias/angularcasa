import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Casa } from '../casa';
import { CasaDTO } from '../casaDTO';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css']
})
export class DetalhesComponent implements OnInit {

  corCasa = '';
  codigoCasa = 0

  constructor(private service: AppService, private router: Router, private rota: ActivatedRoute) { }

  ngOnInit(): void {
    this.codigoCasa = this.rota.snapshot.params['id'];
    if(this.codigoCasa != undefined) {
      this.service.findById(this.codigoCasa).subscribe((resp) => {
        this.corCasa = resp.cor;
      })
    }
  }

  salvar() {
    if(this.codigoCasa > 0) {
      let novaCasa = new Casa(this.codigoCasa, this.corCasa)
      this.service.update(novaCasa).subscribe((resp) => {
        console.log("Casa alterada com sucesso!")
      })
    } else {
      let novaCasa = new CasaDTO(this.corCasa);
      this.service.save(novaCasa).subscribe((retorno) => {
        console.log("Casa criada com sucesso!")
      });
    }
  }

  voltar(): void {
    this.router.navigate([''])
  }

}
