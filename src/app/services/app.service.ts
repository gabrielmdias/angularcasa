import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Casa } from '../casa';
import { CasaDTO } from '../casaDTO';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  baseUrl = environment.baseUrl

  constructor(private http: HttpClient) { }

  findById(id: number): Observable<Casa> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Casa>(url);
  }

  findAll(): Observable<Casa[]> {
    return this.http.get<Casa[]>(this.baseUrl);
  }

  save(casa: CasaDTO) : Observable<Casa> {
    console.log('salvar casa')
    return this.http.post<Casa>(this.baseUrl, casa)
  }

  update(casa: Casa): Observable<Casa> {
    const url = `${this.baseUrl}/${casa.id}`
    return this.http.put<Casa>(url, casa)
  }
}
